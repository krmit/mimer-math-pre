#!/usr/bin/node
/*
 *   Copyright 2017 Magnus Kronnäs
 *
 *   This file is part of math-question program.
 *
 *   math-question is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software 
 *   Foundation, either version 3 of the License, or (at your option) any later 
 *    version.
 *
 *   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY 
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with 
 *   easy-web-framework. If not, see http://www.gnu.org/licenses/.
 */
"use strict"
require("colors");
const yaml = require('js-yaml');
const fs   = require('fs');
const os   = require('os');
const moment = require('moment');
const prompt=require("prompt-sync")();
const sprintf = require('sprintf-js').sprintf;
const mkdirp = require('mkdirp');
var Pie = require("cli-pie");

var argv = require('yargs')
    .usage('Usage: $0')
    .alias('p', 'profile')
    .nargs('p', 1)
    .describe('p', 'Profile to use.')
    .alias('d', 'diagram')
    .describe('d', 'Show some diagrams.')
    .help('h')
    .alias('h', 'help')
    .epilog('copyright 2017')
    .argv;

const dir_name = os.homedir() + '/.math-question';

const excersise = {}
excersise.name="Add10Times5";
excersise.title="Add 1 to 10 - 5 times";

mkdirp.sync(dir_name);

let file_name;
if(argv["profile"]!==undefined) {
	file_name = dir_name + "/"+argv["profile"]+".json";
}
else {
	file_name = dir_name + "/default.json";
}

let data;

try {
  data = JSON.parse(fs.readFileSync(file_name, 'utf8'));
} catch (e) {
  data =createNewUser("data");
}

const number_of_question=5;

console.log(("Redo för frågor "+data.conf.name+"?\n").blue.bold.underline);

const question = function(questionText, answer) {
    let user_answer = Number(prompt(questionText+" "))
    if(user_answer === answer) {
		console.log("OK!".green);
		return 1;
	}
	console.log("Fel".red+"\n"+"Svaret är: "+answer);
	return 0
}

let points=0;

const start_time = moment();
for(let i = 0; i < number_of_question; i++) {
	const number_one = Math.floor(Math.random() *10);
    const number_two = Math.floor(Math.random() *10);
    points+=question(number_one+" + "+number_two+" =",number_one+number_two);
}

const time=moment().diff(start_time);
let log_data={}
log_data.date=(moment(start_time).format("YYYY:MM:DD  hh:mm:ss"));
log_data.excersise=excersise.name;
log_data.points=points;
log_data.time=time;
data.log.push(log_data);
let stats_data=data.stats[excersise.name];
if(stats_data!==undefined) {
	stats_data.count++;
	stats_data.totalPoints+=points;
	if(points > stats_data.points.best) {
	    stats_data.points.best=points;
    }
    
    if(time < stats_data.time.best && points===5) {
	    stats_data.time.best=time;
    }
}
else {
	stats_data={};
	stats_data.count=1;
	stats_data.totalPoints=points;
	stats_data.points={};
	stats_data.points.best=points;
	stats_data.time={};
	if(points===5) {
	    stats_data.time.best=time;
    }
    else {
		stats_data.time.best=null;
	}
	
	data.stats[excersise.name]=stats_data;
}

if(argv["diagram"]) {
console.log(drawPie(points,5-points).toString());
}

console.log("\nPoäng:      " + (points+"/"+number_of_question).bold + (points===5?randomGood():""));
console.log("Tid:   "+(moment(time).format("mm:ss:SS")).bold);
console.log("Totalt antal gånger:   "+stats_data.count);
console.log(sprintf("Medel poäng:           %.1f", stats_data.totalPoints/stats_data.count));
if(stats_data.time.best === time) {
	console.log("Nytt rekord! ✨✨⭐️✨✨".bold.green);
}
if(stats_data.time.best!==null) {
    console.log(("Bästa tiden är:   "+(moment(stats_data.time.best).format("mm:ss:SS")).bold));
}

if(stats_data.time.best === time && points === 5) {
    console.log("Good!\n".green);
}

const data_json = JSON.stringify(data);
fs.writeFile(file_name, data_json);

function createNewUser(name) {
    	let data={};
    	data.conf={};
    	data.conf.name=prompt("Vad är ditt namn? ");
    	data.conf.create=moment().format("YYYY:MM:DD  hh:mm:ss");
    	data.log=[];
    	data.stats={};
    	return data;
}

function drawPie(right, wrong) {
    return new Pie(5, [
    { label: "Right", value: right, color: [ 0, 255, 0] }
  , { label: "Wrong", value: wrong, color: [ 255, 0, 0] }
], {
    legend: true
});
}

function randomGood(){
    switch(Math.floor(Math.random()*4)) {
        case 0:
            return "😆";
            break;
        case 1:
            return "😎";
            break;
        case 2:
            return "😺";
            break;
        case 3:
            return "😋";
            break;
    }
}
